<div align='center'>
    <h1><b>  Active Colloids with DPD Hydrodynamics </b></h1>    
    <p>LAMMPS extension implementing active colloids in an explicit DPD solvent with that self-propel via hydrodynamic squirmer-like force field.</p>
</div>

---

## **ABOUT**


In this LAMMPS extension we simulate colloids, using a raspberry model of rigid DPD particles, that self-propell imposing an axisymmetric force field (for the moment squirmer-like) among its neigboring DPD solvent particles. An equal an opposite force is then applied to the colloids particles providing a net self propulsion thrust if the prescribed force field is asymmetric.

The extension is implemented in two source files (fix\_puller.cpp and fix\_puller.h) that are to be placed in the source folder of LAMMPS. When compiled, an additional fix is available that will enable the functionality described above via a fix style.

https://www.frontiersin.org/articles/10.3389/fphy.2022.926609/full


<br/>

---

## **INSTALLATION**

### Local installation:

Currently only the release folder is ready for public use. The rest is a work in pogress. In this folder you will find a source folder contining various LAMMPS source codes (currently the extension only works in the version lammps\_2Jul2021). Please check the LAMMPS documentation on how to build LAMMPS (https://docs.lammps.org/Build.html).

1\. Download release folder

2\. Go to source

```
cd src/src_lammps-2Jul2021
```

3\. Clean everything and install needed lammps packages

```
make yes-RIGID
make yes-MOLECULE
make yes-MISC
```

4\. Compile serial or mpi lammps executable

```
make serial
```
or
```
make mpi
```

5\. Copy executable to parent folder

```
cp lmp_mpi ../../lmp_mpi
```

<br/>

---

## **USAGE**


There are tow folders `dpd-squirmer` and `dpd-squimers` that provide the necessary input and initial configuration files to run one dpd squirmer in bulk and 50 squirmers in bulk respectively. It is recommended to run with mpi parallelization since the computation is not lightweight and can take a few hours running in 8 - 16 cores.

1\. Go to dpd-squimer folder

```
cd ../../dpd-squirmer
```

2\. Run with 8 cores mpi parallelization

```
mpirun-np 8 ../lmp_mpi -in in.dpd-squirmer.lammps
```

3\. The fix puller command inside the LAMMPS input script is used as follows.

```
fix ${fix_id} all hy/puller ${type_col_cent} ${type_col_fill} ${type_col_solv} ${type_angle} ${cutoff_HYDRO} ${Fp} ${cutoff_COL} ${B1} ${B2}
```

`fix_id`: the name of the fix. See lammps documentation for more.

`type_col_cent`: the type of the particles which are used to compute neighbors and are the source of the force field. 

`type_col_fill`: the type of particles that constitute the body of the colloid and that will receive the reaction force of the applied force field to the solvent.

`type_col_solv`: the type of particles that contitute the solvent.

`type_angle`: the type of the angle style that is used in three colinear particles of the colloid to fix the reference frame of the colloid (and the force field).

`cutoff_HYDRO`: the high cutoff (outer radius) of the force field

`Fp`: the total propulsion force

`cutoff_COL`: the low cutoff (inner radius) of the force field

`B1`: squirmer parameter B\_1. Keep it to 1, right now `Fp` controls this.

`B2`: squirmer parameter B\_2.

<br />

---

## **DEMO**

There is also a small demo in a python jupyter notebook which walks you over these steps and very basic analysis of the simulation output data.

<br />

---

## **KNOWN ISSUES**

1\. Currently the only tested LAMMPS version is `lammps_2Jul2021` in the latest stable version `lammps-23Jun2022` the compulation fails with:
```
../fix_puller.cpp: In member function ‘virtual void LAMMPS_NS::FixPuller::init()’:
../fix_puller.cpp:264:33: error: ‘int LAMMPS_NS::NeighRequest::pair’ is protected within this context
   neighbor->requests[irequest]->pair = 0;
                                 ^~~~
In file included from ../fix_puller.cpp:61:0:
../neigh_request.h:46:7: note: declared protected here
   int pair;    // pair is set by default
       ^~~~
../fix_puller.cpp:265:33: error: ‘int LAMMPS_NS::NeighRequest::fix’ is protected within this context
   neighbor->requests[irequest]->fix = 1;
                                 ^~~
In file included from ../fix_puller.cpp:61:0:
../neigh_request.h:47:7: note: declared protected here
   int fix;
       ^~~
../fix_puller.cpp:266:33: error: ‘int LAMMPS_NS::NeighRequest::half’ is protected within this context
   neighbor->requests[irequest]->half = 0;
                                 ^~~~
In file included from ../fix_puller.cpp:61:0:
../neigh_request.h:55:7: note: declared protected here
   int half;    // half neigh list (set by default)
       ^~~~
../fix_puller.cpp:267:33: error: ‘int LAMMPS_NS::NeighRequest::full’ is protected within this context
   neighbor->requests[irequest]->full = 1;
                                 ^~~~
In file included from ../fix_puller.cpp:61:0:
../neigh_request.h:56:7: note: declared protected here
   int full;    // full neigh list
       ^~~~
../fix_puller.cpp:268:33: error: ‘int LAMMPS_NS::NeighRequest::cut’ is protected within this context
   neighbor->requests[irequest]->cut = 1;
                                 ^~~
In file included from ../fix_puller.cpp:61:0:
../neigh_request.h:83:7: note: declared protected here
   int cut;          // 1 if use a non-standard cutoff length
       ^~~
../fix_puller.cpp:269:33: error: ‘double LAMMPS_NS::NeighRequest::cutoff’ is protected within this context
   neighbor->requests[irequest]->cutoff = cutoff;
                                 ^~~~~~
In file included from ../fix_puller.cpp:61:0:
../neigh_request.h:84:10: note: declared protected here
   double cutoff;    // special cutoff distance for this list
          ^~~~~~
```

2\. If we try to run two simulations in one input script (i.e. call run twice, even without calling `fix hy/puller`, an error pops up 
    `"Colloid atoms were not identified correctly."` When debugging we see that the
    array we build to count the number of atoms of each colloid that each proc has, 
    already has the total number of atoms of each colloid, thus when we launch in 
    more than one proc, since we are doing MPIallreduce SUM, we find at the end the 
    number of atoms per colloid is `Nproc*atoms_per_col`. Maybe this happens because 
    in the source code we implement this in the constructor and it should be implemented
    in a setup method?
