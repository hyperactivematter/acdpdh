/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   16/07/2021.
   Carlos Miguel Barriuso Gutierrez (carbarri@ucm.es)
   José Martín Roca (josema10@ucm.es) 
------------------------------------------------------------------------- */

////////////////////////////////////////////////////////////////////////////
/**************************************************************************

This fix implements an active colloid composed of DPD particles joined together 
by rigid interactions (fix rigid) swimming in a solvent made of DPD particles. 
The colloid is composed of particles of type <type_col_cent> and 
<type_col_fill>. The solvent are particles of type <type_solv>. A force field 
emanating from the <type_col_cent> particles is imposed in the neighboring 
solvent particles. This force field is consistent with the squirmer model and it 
is fixed with the reference frame of the colloid, which is defined by three 
colinear particles that are specified via an angle style. This particles define 
the axis of the colloid that coincides with the axis of symmetry of the force 
field. The solvent neighborhood in which the force field is applied is an 
espherical shell of inner radius <Rc> and outer radius <cutoff>. Afterwards we 
loop over all the solvent particles contained in this region, find the 
nearest colloid particle and apply an equal and opposite reaction force. 
Currently the force field is that of the squirmer model controlled by 
parameters <B1> and <B2>. Currently the force field is normalised so that 
the total resulting force on the colloid is <Pe> and thus <B1> plays no role 
(this will be changed in the future).


NOTES
// We dont need to compute colloid number and number of particles per colloid
// fix rigid already has it in: nbody -> number of colloids and nrigid is a 
// vector containing number of particles of each rigid body. The thing is 
// that they are private variables so we would need to access them with an
// extract() method so we would need to pass as argument to fix_puller the 
// ID of the fix rigid and then apply something similar to the code present 
// in fix deposit.

**************************************************************************/
///////////////////////////////////////////////////////////////////////////


#include "fix_puller.h"
#include <mpi.h>
#include <cmath>
#include <iostream>
#include <cstring>
#include "atom.h"
#include "atom_masks.h"
#include "comm.h"
#include "neighbor.h"
#include "neigh_list.h"
#include "neigh_request.h"
#include "update.h"
#include "force.h"
#include "group.h"
#include "pair.h"
#include "respa.h"
#include "memory.h"
#include "citeme.h"
#include "error.h"
#include "domain.h"

using namespace LAMMPS_NS;
using namespace FixConst;

#define BIG 1.0e20

/* ---------------------------------------------------------------------- */

// CHANGE NAME
FixPuller::FixPuller(LAMMPS *lmp, int narg, char **arg) : 
  Fix(lmp, narg, arg)
{     

  if (narg == 12){

    type_col_cent = utils::inumeric(FLERR, arg[3],false,lmp);
    type_col_fill = utils::inumeric(FLERR, arg[4],false,lmp);    
    type_solv = utils::inumeric(FLERR, arg[5],false,lmp);
    type_angle = utils::inumeric(FLERR, arg[6],false,lmp);  
    cutoff = utils::numeric(FLERR, arg[7],false,lmp);     
    Pe = utils::numeric(FLERR, arg[8],false,lmp); 
    Rc = utils::numeric(FLERR, arg[9],false,lmp); 
    B1 = utils::numeric(FLERR, arg[10],false,lmp);
    B2 = utils::numeric(FLERR, arg[11],false,lmp);

  } else {

    error->all(FLERR, "Illegal fix hy/puller command"); // CHANGE NAME
  
  }
  
  int me = 0; 
  MPI_Comm_rank(world,&me); // "me" is the number of the id of the core executing this line.
  int *type = atom->type;
  tagint *tag = atom->tag;  // This is not used  
  tagint *molecule = atom->molecule;
  bigint natoms = atom->natoms;

  // int nanglelist = 1; // neighbor->nanglelist; // This is a problem since nanglelist comes from neighbor which is not called in this step. So here nanglelist = 0. But if we put it in prereverse, the code hangs o seg faults.
  int local_idx;
  int atom_tag;
  int atoms_per_colloid_old=0;  // This is not used
  int atoms_per_colloid_old_max=0;  // This is not used
  int colloid_idx;
  int counter;
  atoms_per_colloid=0;  // Already declared in .h


  //////////////////////////////////////////////////////////////////////////////
  // Compute number of colloids and check that is = to number of angle types. //
  //////////////////////////////////////////////////////////////////////////////

  num_colloids = 1;

  for (atom_tag = 1; atom_tag <= natoms; ++atom_tag) {

    local_idx = atom->map(atom_tag);

    if (molecule[local_idx] > num_colloids) num_colloids = molecule[local_idx];

  }

  // We pick the biggest num_colloids among all threads.
  MPI_Allreduce(&num_colloids, &num_colloids_max, 1 ,MPI_INT, MPI_MAX, world);

  // The following is needed to (re)redistribute the force we apply to the 
  // solvent particles over the colloid particles, if we want fixed arrays 
  // containing the colloid particles. Another option would be to search for
  // neighbors of the solvent particles.At the moment it only works if all the 
  // colloids have the same number of particles.

  //////////////////////////////////////////////////////
  // Compute number of atoms per colloid and          //
  // check if all colloids have same number of atoms. //
  //////////////////////////////////////////////////////

  // Every proc counts the number atoms of type col-fill in its subdomain,
  // increasing the element of atoms_per_colloid_arr that corresponds to 
  // the col-fill atom colloid_idx. Then the arrays (of every proc) are sumed 
  // and communicated back to all procs with MPI_Allreduce. 

  int *atoms_per_colloid_arr;
  int *atoms_per_colloid_arr_all; 

  atoms_per_colloid_arr = new int[num_colloids_max];
  atoms_per_colloid_arr_all = new int[num_colloids_max];

  memset(atoms_per_colloid_arr, 0, num_colloids_max*sizeof(atoms_per_colloid_arr[0]));
  memset(atoms_per_colloid_arr_all, 0, num_colloids_max*sizeof(atoms_per_colloid_arr_all[0]));

  for (int icol = 0; icol < num_colloids_max; ++icol) {
   for (atom_tag = 1; atom_tag <= natoms; ++atom_tag){
    
      local_idx = atom->map(atom_tag);
      if (molecule[local_idx] == (icol + 1)) atoms_per_colloid_arr[icol]++;

    }
  }  
  
  MPI_Allreduce(&atoms_per_colloid_arr[0], &atoms_per_colloid_arr_all[0], num_colloids_max, MPI_INT, MPI_SUM, world);

  // If one colloid has different number of atoms -> ERROR.
  if (me == 0) {
    for (int i = 0; i < num_colloids_max; i++) {
        if (atoms_per_colloid_arr_all[i] != atoms_per_colloid_arr_all[0])
          error->one(FLERR, "All colloids must have the same number of atoms.");
    }    
  }

  atoms_per_colloid = atoms_per_colloid_arr_all[0]; // counting the center particle.
  
  delete [] atoms_per_colloid_arr;
  delete [] atoms_per_colloid_arr_all;

  // We need to know which atoms belong to each colloid so now we populate array
  // with global idxs of atoms, one row for each colloid.                       

  // Allocate memory.
  memory->create(col_atoms, num_colloids_max, atoms_per_colloid, "hydroprop:col_atoms");
  memory->create(col_atoms_tot, num_colloids_max, atoms_per_colloid, "hydroprop:col_atoms_tot");

  // Initialize elements to 0.
  for (colloid_idx = 0; colloid_idx < num_colloids_max; ++colloid_idx) {
    for (int iatom = 0; iatom < atoms_per_colloid; ++iatom) {

      col_atoms[colloid_idx][iatom] = 0;

    }
  }

  // Populate de array with global ids.
  counter = 0;  
  for (colloid_idx = 0; colloid_idx < num_colloids_max; ++colloid_idx) {

    for (atom_tag = 1; atom_tag <= natoms; ++atom_tag) {

      local_idx = atom->map(atom_tag);

      // If local ixd = -1, type[-1] = 0 and molecule[-1] = 0;

      if ((type[local_idx] == type_col_fill || type[local_idx] == type_col_cent) && molecule[local_idx] == (colloid_idx + 1)) {

        counter = (atom_tag-1) % atoms_per_colloid;

        col_atoms[colloid_idx][counter] = atom_tag;
        
      } 
    }
    counter = 0;
  }

  // Sum up the arrays of each proc, since for coll particles outside subdomain col_atoms = 0.
  MPI_Allreduce(&col_atoms[0][0], &col_atoms_tot[0][0], atoms_per_colloid*num_colloids_max, MPI_INT, MPI_SUM, world);

  // Error managment
  for (colloid_idx = 0; colloid_idx < num_colloids_max; ++colloid_idx) {
    for (int iatom = 0; iatom < atoms_per_colloid; ++iatom) {
      if (col_atoms_tot[colloid_idx][iatom] == 0)
        error->one(FLERR, "Colloid atoms were not identified correctly. Colloid atoms need to be defined at the beginning of the list of atoms of the data file, one colloid after another.");
    }
  }

}

/* ---------------------------------------------------------------------- */

FixPuller::~FixPuller() {

  memory->destroy(col_atoms);
  memory->destroy(col_atoms_tot);

}

/* ---------------------------------------------------------------------- */

int FixPuller::setmask() {

  int mask = 0;
  mask |= PRE_REVERSE;  
  // mask |= POST_FORCE;
  return mask;

}

/* ---------------------------------------------------------------------- */

void FixPuller::init() {

  int me = 0;
  MPI_Comm_rank(world,&me); 

  // FULL neighbor list. See options in neigh_request.h
  
  // NEW SYNTAX!!! bor->add_request(this, NeighConst::REQ_OCCASIONAL);

  // neighbor->add_request(this, NeighConst::REQ_SIZE | NeighConst::REQ_OCCASIONAL);

  // if (n_pre_reverse) {
  //   modify->pre_reverse(eflag,vflag);
  //   timer->stamp(Timer::MODIFY);
  // }

  // // reverse communication of forces

  // if (force->newton) {
  //   comm->reverse_comm();
  //   timer->stamp(Timer::COMM);
  // }

  // // force modifications, final time integration, diagnostics

  // if (n_post_force_any) modify->post_force(vflag);


  int irequest = neighbor->request(this,instance_me);
  neighbor->requests[irequest]->pair = 0;
  neighbor->requests[irequest]->fix = 1;
  neighbor->requests[irequest]->half = 0;
  neighbor->requests[irequest]->full = 1;
  neighbor->requests[irequest]->cut = 1;
  neighbor->requests[irequest]->cutoff = cutoff;

}

/* ---------------------------------------------------------------------- */

void FixPuller::init_list(int /*id*/, NeighList *ptr)
{
  int me = 0;
  MPI_Comm_rank(world,&me); 

  list = ptr;

}

/* ---------------------------------------------------------------------- */


void FixPuller::setup(int vflag) {
  int me = 0;
  MPI_Comm_rank(world,&me); 

}

/* ---------------------------------------------------------------------- */

void FixPuller::setup_pre_reverse(int eflag, int vflag) {

  int me = 0;
  MPI_Comm_rank(world,&me);

  int nanglelist = neighbor->nanglelist;
  int nanglelist_tot = 0;
  MPI_Allreduce(&nanglelist, &nanglelist_tot, 1, MPI_INT, MPI_SUM, world);

  // We need to use error->one not error->all when using MPI, look error.cpp).
  if (num_colloids_max != nanglelist_tot) {
      error->one(FLERR, "Number of angles different from number of molecules (colloids). Solvent particles must have molecule index = 0!!!");
  }  

  pre_reverse(eflag,vflag);

}

/* ---------------------------------------------------------------------- */

void FixPuller::pre_reverse(int vflag, int /*vflag*/){

 int me = 0;
 MPI_Comm_rank(world,&me);


  /////////////////////////////////////////
  // ¡¡¡ LIMPIAR VARIABLES NO USADAS !!! //
  /////////////////////////////////////////
  
  // int ntimestep = update->ntimestep;
  // int i,j,ii,jj,inum,jnum,itype,jtype,atype;
  // int *ilist, *jlist,*numneigh, **firstneigh;
  // bigint natoms = atom->natoms;
  // int *mask = atom->mask;
  // double Fa, theta;
  // double Fa_x, Fa_y, Fa_z;
  // double prod_scal, cos_theta_2;
  // int newton_bond = force->newton_bond;
  // int nghost = atom->nghost;
  // int nlocal = atom->nlocal;
  // int col_part_global_id_min;


  int i,j,jj,jnum,itype,jtype,atype;
  double xtmp, ytmp, ztmp;
  int *jlist,*numneigh, **firstneigh;

  double delx,dely,delz;
  double ori_x,ori_y,ori_z;  
  double oriXuni,oriYuni,oriZuni;
  double ori_mod;

  double rsq,r;

  double **x = atom->x;
  double **f = atom->f;
  int *type = atom->type;
  tagint *tag = atom->tag;
  tagint *molecule = atom->molecule;

  int max_num_neigh=20000;

  double norma_x, norma_y, norma_z;
  double norma;

  double weight_factor_x[max_num_neigh];
  double weight_factor_y[max_num_neigh];
  double weight_factor_z[max_num_neigh];

  int i1,i2,i3,n;
  int **anglelist = neighbor->anglelist;
  int nanglelist = neighbor->nanglelist;


  double delx_solv_colpart = 0;
  double dely_solv_colpart = 0;
  double delz_solv_colpart = 0;
  double rsq_solv_colpart = 0;
  double rsq_solv_colpart_min;

  int col_part;
  int col_part_local_idx;
  int col_part_local_idx_min;
  int colidx;

  numneigh = list->numneigh; 	// # of J neighbors for each I atom
  firstneigh = list->firstneigh; // ptr to 1st J int value of each I atom

/////////////////////////////////////////////////////////////////

  double *boxlo = domain->boxlo;
  double *boxhi = domain->boxhi;  
  double x_size = boxhi[0] - boxlo[0];
  double y_size = boxhi[1] - boxlo[1];
  double z_size = boxhi[2] - boxlo[2];


  //////////////////
  // COLLOID LOOP //
  //////////////////

  // Loop Angle list (local to each proc).  
  for (n = 0; n < nanglelist; n++) { //[Loop angles]

    atype = anglelist[n][3];                              /// Type of angle-bond

    if(atype == type_angle){

      /// Index of particles in the angle
      i1 = anglelist[n][0];
      i2 = anglelist[n][1];
      i3 = anglelist[n][2];

      // Colloid orientation
      ori_x = x[i3][0] - x[i1][0];
      ori_y = x[i3][1] - x[i1][1];
      ori_z = x[i3][2] - x[i1][2];

      // From local index to tag (global ID)
      int tagi1 = tag[i1];
      int tagi2 = tag[i2];
      int tagi3 = tag[i3];

      // PBCs
      if (ori_x >   x_size * 0.5) ori_x = ori_x - x_size;
      if (ori_x <= -x_size * 0.5) ori_x = ori_x + x_size;
      if (ori_y >   y_size * 0.5) ori_y = ori_y - y_size;
      if (ori_y <= -y_size * 0.5) ori_y = ori_y + y_size;
      if (ori_z >   z_size * 0.5) ori_z = ori_z - z_size;
      if (ori_z <= -z_size * 0.5) ori_z = ori_z + z_size;

      // Unitary orientation vector
      ori_mod = sqrt(ori_x*ori_x + ori_y*ori_y + ori_z*ori_z);

      oriXuni = ori_x/ori_mod;
      oriYuni = ori_y/ori_mod;
      oriZuni = ori_z/ori_mod;

      // Check particle is of type_co_cent
      itype = type[i2];

      if(itype == type_col_cent){ 

        // Get center particle position
        xtmp = x[i2][0];
        ytmp = x[i2][1];
        ztmp = x[i2][2];
  
        // Get its number neighbors and local idxs of neighbors.
        jnum = numneigh[i2];
        jlist = firstneigh[i2];

        // This will be used to normalise the force field afterwards.
        norma_x = 0;
        norma_y = 0;
        norma_z = 0;


        ///////////////////
        // NEIGHBOR LOOP //
        ///////////////////

        for (jj = 0; jj < jnum; jj++) { /// [Loop vecinos jj]
       
          j = jlist[jj];
          j &= NEIGHMASK;     // This removes the bits that encode, whether a j atom of an i-j pair is a "special" pair, i.e. part of a topological pair, angle or dihedral.
          jtype = type[j];

          // Cogemos solo los vecinos del tipo sobre el que queremos resdistribuir
          // la fuerza y calculamos el peso con el que vamos a redistribuir. Si no
          // es de ese tipo departicula el peso lo ponemos a cero
          if (jtype == type_solv){

            delx = x[j][0]-xtmp;
	          dely = x[j][1]-ytmp;
	          delz = x[j][2]-ztmp;
	          rsq = delx*delx + dely*dely + delz*delz;
            r=sqrt(rsq);

            // If it is in range (further than hydro cutoff but outside the colloid.
            if (r < cutoff && r > Rc){

              weight_factor_x[jj] = (delx*dely*oriYuni + delx*delz*oriZuni - oriXuni*(pow(dely,2) + pow(delz,2)))*pow(pow(delx,2) + pow(dely,2) + pow(delz,2),-1.5)*pow(pow(oriXuni,2) + pow(oriYuni,2) + pow(oriZuni,2),-1) * (B2*(delx*oriXuni + dely*oriYuni + delz*oriZuni) + B1*pow((pow(delx,2) + pow(dely,2) + pow(delz,2))*(pow(oriXuni,2) + pow(oriYuni,2) + pow(oriZuni,2)),0.5));
              weight_factor_y[jj] = (dely*(delx*oriXuni + delz*oriZuni) - oriYuni*(pow(delx,2) + pow(delz,2)))*pow(pow(delx,2) + pow(dely,2) + pow(delz,2),-1.5)*pow(pow(oriXuni,2) + pow(oriYuni,2) + pow(oriZuni,2),-1)*(B2*(delx*oriXuni + dely*oriYuni + delz*oriZuni) + B1*pow((pow(delx,2) + pow(dely,2) + pow(delz,2))*(pow(oriXuni,2) + pow(oriYuni,2) + pow(oriZuni,2)),0.5));
              weight_factor_z[jj] = (delz*(delx*oriXuni + dely*oriYuni) - oriZuni*(pow(delx,2) + pow(dely,2)))*pow(pow(delx,2) + pow(dely,2) + pow(delz,2),-1.5)*pow(pow(oriXuni,2) + pow(oriYuni,2) + pow(oriZuni,2),-1)*(B2*(delx*oriXuni + dely*oriYuni + delz*oriZuni) + B1*pow((pow(delx,2) + pow(dely,2) + pow(delz,2))*(pow(oriXuni,2) + pow(oriYuni,2) + pow(oriZuni,2)),0.5));

              norma_x += weight_factor_x[jj];
              norma_y += weight_factor_y[jj];
              norma_z += weight_factor_z[jj];

            } else {

              weight_factor_x[jj] = 0.0;
              weight_factor_y[jj] = 0.0;
              weight_factor_z[jj] = 0.0;

            }
          
          // Es particula de relleno del coloide   
          } else if (jtype == type_col_fill) {

              weight_factor_x[jj] = 0.0;
              weight_factor_y[jj] = 0.0;
              weight_factor_z[jj] = 0.0;

          // Es particula central
          } else if (jtype == type_col_cent) {  // Solo debría poder ser otra particula central

              weight_factor_x[jj] = 0.0;
              weight_factor_y[jj] = 0.0;
              weight_factor_z[jj] = 0.0;

          // Es otro tipo de particula (pared por ejemplo). De momento no implementado.
          } else {

              weight_factor_x[jj] = 0.0;
              weight_factor_y[jj] = 0.0;
              weight_factor_z[jj] = 0.0;

          }

        }


        /////////////////////////////////////////////////
        // Apply forces to neighbor solvent particles. //
        /////////////////////////////////////////////////

        // We compute the norm of the resulting vector from adding all redistributed vectors.
        norma = sqrt( pow(norma_x,2) + pow(norma_y,2) + pow(norma_z,2) );
        
        for (jj = 0; jj < jnum; jj++) { // [FOR  jj]

          j = jlist[jj];    // Local idx of neighbor
          j &= NEIGHMASK;
          jtype = type[j];

          delx = x[j][0]-xtmp;
          dely = x[j][1]-ytmp;
          delz = x[j][2]-ztmp;
          rsq = delx*delx + dely*dely + delz*delz;
          r=sqrt(rsq);

          // If particle is solvent and it is in range.
          if (jtype == type_solv && r < cutoff && r > Rc) { // [If in range solv particle]

            // Apply force to solvent.
            f[j][0] += Pe*weight_factor_x[jj]/norma;  
            f[j][1] += Pe*weight_factor_y[jj]/norma;
            f[j][2] += Pe*weight_factor_z[jj]/norma;

            // Search nearest colloid particle and apply equal and opposite force.
            colidx = molecule[i2]-1;
            col_part_local_idx_min = atom->map(col_atoms_tot[colidx][0]);
            rsq_solv_colpart_min = BIG;

            for (col_part = 0; col_part < atoms_per_colloid; ++col_part){
              
              // Select colloid particle
              col_part_local_idx = atom->map(col_atoms_tot[colidx][col_part]);

              // Exclude center particles from redistribution.
              if (type[col_part_local_idx] != type_col_cent) {
              
                // Check its ghosts (when particle is outside subdomain col_part_local_idx = -1 ).
                while (col_part_local_idx >= 0) {

                  delx_solv_colpart = x[col_part_local_idx][0] - x[j][0];
                  dely_solv_colpart = x[col_part_local_idx][1] - x[j][1];
                  delz_solv_colpart = x[col_part_local_idx][2] - x[j][2];

                  rsq_solv_colpart = pow(delx_solv_colpart,2) + pow(dely_solv_colpart,2) + pow(delz_solv_colpart,2);

                  if (rsq_solv_colpart < rsq_solv_colpart_min){

                    rsq_solv_colpart_min = rsq_solv_colpart;
                    col_part_local_idx_min = col_part_local_idx;

                  } 

                  // Go to the next ghost until col_part_local_idx = -1.
                  col_part_local_idx = atom->sametag[col_part_local_idx];

                }
              }
            }

            f[col_part_local_idx_min][0] -= Pe*weight_factor_x[jj]/norma;  
            f[col_part_local_idx_min][1] -= Pe*weight_factor_y[jj]/norma;
            f[col_part_local_idx_min][2] -= Pe*weight_factor_z[jj]/norma;            

          } // If in range solv particle
        } // For neighbors
      } // If type_col_cent
    } // If type_angle
  } // Loop angles
}


