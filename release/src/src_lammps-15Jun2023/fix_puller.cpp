/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   16/07/2021.
   José Martín Roca (josema10@ucm.es) and Carlos Miguel Barriuso Gutierrez (carbarri@ucm.es)
   Puller and Pusher project
------------------------------------------------------------------------- */

////////////////////////////////////////////////////////////////////////////
/**************************************************************************
El modelo de redistribucion que tenemos ahora es:

Tenemos una particula central unida a dos particulas extremos mediante un 
enlace de tipo angle
                      1  --  2  -- 1

esto nos proporciona un eje sobre el que aplicar la fuerza activa que queremos
incluso si esta estructura rota seguimos pudiendo obtener una direccion de 
aplicacion mediante este enlace. La fuerza se aplica en la particula central
que en nuestras cofiguraciones la llamamos de tipo 2, y se aplica en la direccion
dada por sus vecinos a los que esta unido con el angle

Notacion usual:

Particula tipo 1 = Particula que forma parte de la estructura pero no se le aplica
ninguna fuerza en ningún momento salvo las pair wise. Tambien tienen interaccion
con muelle o rigid dependiendo del caso

Particula tipo 2 = Particula que forma parte de la etructura y sobre la que se le
aplica la fuerza activa además de la fuerza pair wise. Tambien tienen interaccion
con muelle dependiendo del caso

Particula tipo 3 = Particula sobre la que se redistribuye la fuerza activa aplicada
en las particulas de tipo 2 (solvente)

Bond de tipo 1 = Para los coloides no hay, para los polimeros es el muelle armónico
que los une dos a dos

Bond de tipo 2 = Es el que nos proporciona el bond activo, que es un "angle style",
que une los átomos de 3 en tres (para formar el angulo)

Estructuras:

-> En el caso del coloide tenemos (visto en 2D) este estructura RIGIDA (usando la 
libreria 'rigid')

			   1           
			1  1  1        
		      1 1--2--1 1                    
			1  1  1
                           1

Necesitamos incluirle dos bonds auxiliares para generar el eje activo que vamos
a usar, para que así la direccion se mueva junto a la estructura

-> En el caso del polimero tenemos (visto en 2D) esta estructura formada por beads
unidos entre sí por dos muelles, un muelle armonico + el muelle auxiliar que actua
como eje activo para aplicar la fuerza, aunque con el armónico puede valer


       1 == 2 =          = 2 =
               = 2 == 2 =     = 2 == 1


**************************************************************************/
///////////////////////////////////////////////////////////////////////////


#include "fix_puller.h"
#include <mpi.h>
#include <cmath>
#include <iostream>
#include <cstring>
#include "atom.h"
#include "atom_masks.h"
#include "comm.h"
#include "neighbor.h"
#include "neigh_list.h"
#include "neigh_request.h"
#include "update.h"
#include "force.h"
#include "group.h"
#include "pair.h"
#include "respa.h"
#include "memory.h"
#include "citeme.h"
#include "error.h"
#include "domain.h"

using namespace LAMMPS_NS;
using namespace FixConst;

#define BIG 1.0e20

/* ---------------------------------------------------------------------- */

// Change name
FixPuller::FixPuller(LAMMPS *lmp, int narg, char **arg) : 
  Fix(lmp, narg, arg)
{     

  // printf("\n\n 1 --------------------------\n\n");

  if (narg == 12){

    // strcpy(idrigid, "intCol");
    type_col_cent = utils::inumeric(FLERR, arg[3],false,lmp);
    type_col_fill = utils::inumeric(FLERR, arg[4],false,lmp);    
    type_solv = utils::inumeric(FLERR, arg[5],false,lmp);
    type_angle = utils::inumeric(FLERR, arg[6],false,lmp);  
    cutoff = utils::numeric(FLERR, arg[7],false,lmp);     
    Pe = utils::numeric(FLERR, arg[8],false,lmp); 
    Rc = utils::numeric(FLERR, arg[9],false,lmp); // cutoff_Radi = Radio del coloide por si hay particulas de solvente dentro
    B1 = utils::numeric(FLERR, arg[10],false,lmp);
    B2 = utils::numeric(FLERR, arg[11],false,lmp);

    // No necesitamos calcular num de col y parts per col! 
    // rigid ya lo tiene: nbody -> numero de coloides y 
    // nrigid es vector conteniendo num de parts de cada 
    // rigid body. lo q pasa es que son privadas, hay que
    // acceder a ellas con extract() para lo cual necesit
    // amos pasar a fix_puller el ID de fix rigid como arg
    // umento y luego aplicar algo parecido al codigo de 
    // abajo sacado de fix_deposit.

    // Para sacar nbody y nrigid habria que añadir al metodo
    // extract de fix_rigid.cpp unas lineas para que los pue
    // da extraer. Ya tiene para extraer body que es un atom
    // vector conteniendo el id del body de cada atomo, osea
    // que podriamos extraer body y sacar nbody y nrigid de 
    // el. Ademas teniendo body ya podemos construir el array 
    // con los atomos de cada coloide.

  // idrigid = nullptr;
  //   int n = strlen(arg[iarg+1]) + 1;
  //     delete [] idrigid;
  //     idrigid = new char[n];
  //     strcpy(idrigid,arg[iarg+1]);

  //       fixrigid = nullptr;
  // if (rigidflag) {
  // int ifix = modify->find_fix(idrigid);
  //   if (ifix < 0) error->all(FLERR,"Fix deposit rigid fix does not exist");
  //   fixrigid = modify->fix[ifix];
  // int tmp;
  // fixrigid->extract("nbody",tmp));
  // }
  // add   delete [] idrigid; to detroyer.


  } else {

    error->all(FLERR, "Illegal fix hy/puller command");                 // CAMBIAR NOMBRE
  
  }
  
  int me = 0;
  MPI_Comm_rank(world,&me); // "me" is the number of the id of the core executing this line.
  int *type = atom->type;
  tagint *tag = atom->tag;  
  tagint *molecule = atom->molecule;
  bigint natoms = atom->natoms;

  // int nanglelist = 1; // neighbor->nanglelist; // This is a problem since nanglelist comes from neighbor which is not called in this step. So here nanglelist = 0. But if we put it in prereverse, the code hangs o seg faults.
  int local_idx;
  int atom_tag;
  int atoms_per_colloid_old=0;
  int atoms_per_colloid_old_max=0;  
  int colloid_idx;
  int counter;
  atoms_per_colloid=0;


  //////////////////////////////////////////////////////////////////////////////
  // Compute number of colloids and check that is = to number of angle types. //
  //////////////////////////////////////////////////////////////////////////////

  num_colloids = 1;

  for (atom_tag = 1; atom_tag <= natoms; ++atom_tag) {

    local_idx = atom->map(atom_tag);

    if (molecule[local_idx] > num_colloids) num_colloids = molecule[local_idx];

  }


  // Cogemos el num_colloids mas grande entre todos los procs.
  MPI_Allreduce(&num_colloids, &num_colloids_max, 1 ,MPI_INT, MPI_MAX, world);

  // DEBUG 1 printf("\n\n(core = %d) LOOP END   - num_colloids = %d,  num_colloids_max = %d\n\n", me, num_colloids, num_colloids_max);

  // Lo siguiente es necesario para (re)redistribuir la fuerza que aplicamos a las particulas de solv-
  // ente sobre las particulas del coloide, si queremos arrays fijos que contengan las part-
  // ículas de coloide. Otra opción sería buscar vecinos de las particulas de solvente.
  // De momento solo funciona si todos los coloides tienen el mismo numero de particulas.

  //////////////////////////////////////////////////////
  // Compute number of atoms per colloid and          //
  // check if all colloids have same number of atoms. //
  //////////////////////////////////////////////////////

  // Every proc counts the number atoms of type col-fill in its subdomain,
  // increasing the element of atoms_per_colloid_arr that corresponds to 
  // the col-fill atom colloid_idx. Then the arrays (of every proc) are sumed 
  // and communicated back to all procs with MPI_Allreduce. 

  // DEBUG 1 printf("\n(core = %d) FIRST COLLOID\n", me);

  int *atoms_per_colloid_arr;
  int *atoms_per_colloid_arr_all; 

  atoms_per_colloid_arr = new int[num_colloids_max];
  atoms_per_colloid_arr_all = new int[num_colloids_max];

  memset(atoms_per_colloid_arr, 0, num_colloids_max*sizeof(atoms_per_colloid_arr[0]));
  memset(atoms_per_colloid_arr_all, 0, num_colloids_max*sizeof(atoms_per_colloid_arr_all[0]));

  for (int icol = 0; icol < num_colloids_max; ++icol) {
   for (atom_tag = 1; atom_tag <= natoms; ++atom_tag){
    
      local_idx = atom->map(atom_tag);
      if (molecule[local_idx] == (icol + 1)) atoms_per_colloid_arr[icol]++;

    }
    // printf("\n(core = %d) atoms_per_colloid_arr[%d] = %d\n", me, icol, atoms_per_colloid_arr[icol]);

  }  
  
  MPI_Allreduce(&atoms_per_colloid_arr[0], &atoms_per_colloid_arr_all[0], num_colloids_max, MPI_INT, MPI_SUM, world);

  // If one colloid has different number of atoms -> ERROR.
  if (me == 0) {
    for (int i = 0; i < num_colloids_max; i++) {
        if (atoms_per_colloid_arr_all[i] != atoms_per_colloid_arr_all[0])
          error->one(FLERR, "All colloids must have the same number of atoms.");
    }    
  }

  atoms_per_colloid = atoms_per_colloid_arr_all[0]; // counting the center particle.
  
  delete [] atoms_per_colloid_arr;
  delete [] atoms_per_colloid_arr_all;

  // DEBUG 1 printf("\n\n 5.5.3 -------------------------- me = %d\n",me);


  /////////////////////////////////////////////////////////////////////////////////
  // We need to know which atoms belong to each colloid so now we populate array //
  // with global idxs of atoms, one row for each colloid.                        //
  /////////////////////////////////////////////////////////////////////////////////

  // Allocate memory.
  memory->create(col_atoms, num_colloids_max, atoms_per_colloid, "hydroprop:col_atoms");
  memory->create(col_atoms_tot, num_colloids_max, atoms_per_colloid, "hydroprop:col_atoms_tot");

  // Initialize elements to 0.
  for (colloid_idx = 0; colloid_idx < num_colloids_max; ++colloid_idx) {
    for (int iatom = 0; iatom < atoms_per_colloid; ++iatom) {

      col_atoms[colloid_idx][iatom] = 0;

    }
  }

  // Populate de array with global ids.
  counter = 0;  
  for (colloid_idx = 0; colloid_idx < num_colloids_max; ++colloid_idx) {

    for (atom_tag = 1; atom_tag <= natoms; ++atom_tag) {

      local_idx = atom->map(atom_tag);

      // if local_idx != -1 ? This is not needed since if lixd = -1, type[-1] = 0 and molecule[-1] = 0;

      if ((type[local_idx] == type_col_fill || type[local_idx] == type_col_cent) && molecule[local_idx] == (colloid_idx + 1)) {

        counter = (atom_tag-1) % atoms_per_colloid;

        col_atoms[colloid_idx][counter] = atom_tag;
        
      } 
    }
    counter = 0;
  }

  // Sum up the arrays of each proc, since for coll particles outside subdomain col_atoms = 0.
  MPI_Allreduce(&col_atoms[0][0], &col_atoms_tot[0][0], atoms_per_colloid*num_colloids_max, MPI_INT, MPI_SUM, world);

  // Error managment
  for (colloid_idx = 0; colloid_idx < num_colloids_max; ++colloid_idx) {
    for (int iatom = 0; iatom < atoms_per_colloid; ++iatom) {
      if (col_atoms_tot[colloid_idx][iatom] == 0)
        error->one(FLERR, "Colloid atoms were not identified correctly. Colloid atoms need to be defined at the beginning of the list of atoms of the data file, one colloid after another.");
    }
  }

  // DEBUG
  // for (colloid_idx = 0; colloid_idx < num_colloids_max; ++colloid_idx) {
  //   for (atom_tag = 0; atom_tag < atoms_per_colloid; ++atom_tag) {
  //     printf("(core %d) col_atoms[%d][%d] = %d\n", me, colloid_idx, atom_tag, col_atoms[colloid_idx][atom_tag]);
  //   }
  // }


  // for (colloid_idx = 0; colloid_idx < num_colloids_max; ++colloid_idx) {
  //   for (atom_tag = 0; atom_tag < atoms_per_colloid; ++atom_tag) {
  //     printf("(core %d) col_atoms_tot[%d][%d] = %d\n", me, colloid_idx, atom_tag, col_atoms_tot[colloid_idx][atom_tag]);
  //   }
  // }

  // DEBUG 1 printf("\n\n(core %d) 5.5.4 -------------------------- \n",me);  



}

/* ---------------------------------------------------------------------- */

FixPuller::~FixPuller() {
// DEBUG 1 printf("\n\n 2 --------------------------\n\n");

  memory->destroy(col_atoms);
  memory->destroy(col_atoms_tot);

}

/* ---------------------------------------------------------------------- */

int FixPuller::setmask() {
// DEBUG 1 printf("\n\n 3 --------------------------\n\n");

  int mask = 0;
  mask |= PRE_REVERSE;  
  // mask |= POST_FORCE;
  return mask;

}

/* ---------------------------------------------------------------------- */

void FixPuller::init() {

  int me = 0;
  MPI_Comm_rank(world,&me); // "me" is the number of the id of the core executing this line.
  // printf("\n\n(core = %d) 4 --------------------------\n\n", me);

  // FULL neighbor list. Estas opciones se pueden mirar en neigh_request.h
  int irequest = neighbor->request(this,instance_me);
  neighbor->requests[irequest]->pair = 0;
  neighbor->requests[irequest]->fix = 1;
  neighbor->requests[irequest]->half = 0;
  neighbor->requests[irequest]->full = 1;
  neighbor->requests[irequest]->cut = 1;
  neighbor->requests[irequest]->cutoff = cutoff;

}

/* ---------------------------------------------------------------------- */

void FixPuller::init_list(int /*id*/, NeighList *ptr)
{
  int me = 0;
  MPI_Comm_rank(world,&me); // "me" is the number of the id of the core executing this line.

  // DEBUG 1 printf("\n\n(core %d) 5 INIT LIST --------------------------\n\n", me);

  list = ptr;

}


/* ---------------------------------------------------------------------- */


void FixPuller::setup(int vflag) {
  int me = 0;
  MPI_Comm_rank(world,&me); // "me" is the number of the id of the core executing this line.

  
  // DEBUG 1 printf("\n\n(core %d) 5 SETUP --------------------------\n\n", me); 

}





void FixPuller::setup_pre_reverse(int eflag, int vflag) {

  int me = 0;
  MPI_Comm_rank(world,&me); // "me" is the number of the id of the core executing this line.

  
  // DEBUG 1 printf("\n\n(core %d) 6 --------------------------\n\n", me);

  int nanglelist = neighbor->nanglelist;
  int nanglelist_tot = 0;
  MPI_Allreduce(&nanglelist, &nanglelist_tot, 1, MPI_INT, MPI_SUM, world);

  // DEBUG 1 printf("(core %d) Number of (local) angles: %d, Number of (total) angles: %d, Number of colloids: %d\n", me, nanglelist, nanglelist_tot, num_colloids_max);    

  // Si descomentamos esto no se cuelga (esto es porque en MPI tenemos que usar error->one!! no error->all, mira error.cpp), pero falla porque aqui no se ha llamado aun a la neighborlist y nanglelist = 0.
  if (num_colloids_max != nanglelist_tot) {
      error->one(FLERR, "Number of angles different from number of molecules (colloids). Solvent particles must have molecule index = 0!!!");
  }  

  pre_reverse(eflag,vflag);

}

/* ---------------------------------------------------------------------- */

// void FixPuller::post_force(int vflag){
void FixPuller::pre_reverse(int vflag, int /*vflag*/){

 int me = 0;
 MPI_Comm_rank(world,&me); // "me" is the number of the id of the core executing this line.


  /////////////////////////////////////////
  // ¡¡¡ LIMPIAR VARIABLES NO USADAS !!! //
  /////////////////////////////////////////
  
  int ntimestep = update->ntimestep;

  // printf("\n\n(core %d) STEP = %d |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n", me, ntimestep);

  int i,j,ii,jj,inum,jnum,itype,jtype,atype;
  double xtmp, ytmp, ztmp;
  int *ilist, *jlist,*numneigh, **firstneigh;

  double delx,dely,delz;
  double ori_x,ori_y,ori_z;  
  double oriXuni,oriYuni,oriZuni;
  double ori_mod;

  double rsq,r;

  bigint natoms = atom->natoms;
  double **x = atom->x;
  double **f = atom->f;
  int *mask = atom->mask;
  int *type = atom->type;
  tagint *tag = atom->tag;
  
  tagint *molecule = atom->molecule;

  double Fa, theta;
  double Fa_x, Fa_y, Fa_z;
  int max_num_neigh=20000;
  double prod_scal, cos_theta_2;

  double norma_x, norma_y, norma_z;
  double norma;

  double weight_factor_x[max_num_neigh];
  double weight_factor_y[max_num_neigh];
  double weight_factor_z[max_num_neigh];

  int i1,i2,i3,n;
  int **anglelist = neighbor->anglelist;
  int nanglelist = neighbor->nanglelist;

  // DEBUG 1 printf("\n(core %d) nanglelist = %d\n", me, nanglelist);    

  int newton_bond = force->newton_bond;
  int nghost = atom->nghost;
  int nlocal = atom->nlocal;

  double delx_solv_colpart = 0;
  double dely_solv_colpart = 0;
  double delz_solv_colpart = 0;
  double rsq_solv_colpart = 0;
  double rsq_solv_colpart_min;

  int col_part;
  int col_part_local_idx;
  int col_part_local_idx_min;
  int col_part_global_id_min;
  int colidx;


  // // DEBUG 1 printf("Numero de ghosts (nghost):  %d\n", nghost);


  numneigh = list->numneigh; 	// # of J neighbors for each I atom
  firstneigh = list->firstneigh;// ptr to 1st J int value of each I atom

  // Descomentar esto aqui hace que se quede colgao.
  // if (num_colloids != nanglelist) {
  //     error->all(FLERR, "Number of angles different from number of molecules (colloids). Solvent particles must have molecule index = 0!!!");
  // }

/////////////////////////////////////////////////////////////////

  double *boxlo = domain->boxlo;
  double *boxhi = domain->boxhi;  
  double x_size = boxhi[0] - boxlo[0];
  double y_size = boxhi[1] - boxlo[1];
  double z_size = boxhi[2] - boxlo[2];

  // DEBUG 1 printf("(core %d)\n",me);
  // DEBUG 1 printf("\n\n(core %d) LOOP ANGLE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n\n", me);

  // Loop Angle list (local to each proc).  
  for (n = 0; n < nanglelist; n++) { //[Loop angles]

    // DEBUG 1 printf("(core %d)\n",me);
    // DEBUG 1 printf("\n\n(core %d) 7.1 LOOP ANGLE n = %d -----------------------------\n\n", me, n);

    atype = anglelist[n][3];                              /// Tipo de angle-bond

    if(atype == type_angle){                               /// Checkeamos que el bond sea del tipo "bond activo"

      /// Index de las particulas involucradas en el bond
      i1 = anglelist[n][0];
      i2 = anglelist[n][1];
      i3 = anglelist[n][2];

      // Orientación del coloide
      ori_x = x[i3][0] - x[i1][0];
      ori_y = x[i3][1] - x[i1][1];
      ori_z = x[i3][2] - x[i1][2];

      // From local index to tag (global ID)
      int tagi1 = tag[i1];
      int tagi2 = tag[i2];
      int tagi3 = tag[i3];

      // DEBUG 1 printf("(core %d)   Axis particles gid (lixd). i1: %d(%d), i2: %d(%d), i3 %d(%d) \n", me, tagi1, i1, tagi2, i2, tagi3, i3);

      // From tag (global ID) to local index
      // int mapi1 = atom->map(tagi1);
      // int mapi2 = atom->map(tagi2);
      // int mapi3 = atom->map(tagi3);

      // Checkear si se necesitan las pbc
      if (ori_x >   x_size * 0.5) ori_x = ori_x - x_size;
      if (ori_x <= -x_size * 0.5) ori_x = ori_x + x_size;
      if (ori_y >   y_size * 0.5) ori_y = ori_y - y_size;
      if (ori_y <= -y_size * 0.5) ori_y = ori_y + y_size;
      if (ori_z >   z_size * 0.5) ori_z = ori_z - z_size;
      if (ori_z <= -z_size * 0.5) ori_z = ori_z + z_size;

      // Vector orientación unitario
      ori_mod = sqrt(ori_x*ori_x + ori_y*ori_y + ori_z*ori_z);

      oriXuni = ori_x/ori_mod;
      oriYuni = ori_y/ori_mod;
      oriZuni = ori_z/ori_mod;

      // Checkeamos que la particula es de tipo 2 que son las centrales.
      itype = type[i2];

      if(itype == type_col_cent){  /// [If type_col_cent]

        /// Tomamos su posicion y pedimos la lista de vecinos
        xtmp = x[i2][0];
        ytmp = x[i2][1];
        ztmp = x[i2][2];
  
        // Numero de vecinos y array con los local idx de los vecinos.
        jnum = numneigh[i2];
        jlist = firstneigh[i2];

        // DEBUG 1 printf("(core %d)   i2. Type: %d, Pos: (%f, %f, %f), numneigh: %d\n", me, itype, xtmp, ytmp, ztmp, jnum);

          // SACAR NUM VECINOS A UN ARCHIVO
          // FILE *fp;
          // fp = fopen("numneigh.dat","a+");
          // f// DEBUG 1 printf(fp,"%d %d\n", ntimestep, jnum);
          // fclose(fp);

        // La norma se usa para posteriormente normalizar la redistirbucion
        norma_x = 0;
        norma_y = 0;
        norma_z = 0;


        ///////////////////
        // NEIGHBOR LOOP //
        ///////////////////

        for (jj = 0; jj < jnum; jj++) { /// [Loop vecinos jj]
       
          j = jlist[jj];
          j &= NEIGHMASK;     // This removes the bits that encode, whether a j atom of an i-j pair is a "special" pair, i.e. part of a topological pair, angle or dihedral.
          jtype = type[j];

          // Cogemos solo los vecinos del tipo sobre el que queremos resdistribuir
          // la fuerza y calculamos el peso con el que vamos a redistribuir. Si no
          // es de ese tipo departicula el peso lo ponemos a cero
          if (jtype == type_solv){

            delx = x[j][0]-xtmp;
	          dely = x[j][1]-ytmp;
	          delz = x[j][2]-ztmp;
	          rsq = delx*delx + dely*dely + delz*delz;
            r=sqrt(rsq);

            // Si esta en rango (ni más allá del rango hidrodinamico ni dentro del coloide).
            if (r < cutoff && r > Rc){

              weight_factor_x[jj] = (delx*dely*oriYuni + delx*delz*oriZuni - oriXuni*(pow(dely,2) + pow(delz,2)))*pow(pow(delx,2) + pow(dely,2) + pow(delz,2),-1.5)*pow(pow(oriXuni,2) + pow(oriYuni,2) + pow(oriZuni,2),-1) * (B2*(delx*oriXuni + dely*oriYuni + delz*oriZuni) + B1*pow((pow(delx,2) + pow(dely,2) + pow(delz,2))*(pow(oriXuni,2) + pow(oriYuni,2) + pow(oriZuni,2)),0.5));
              weight_factor_y[jj] = (dely*(delx*oriXuni + delz*oriZuni) - oriYuni*(pow(delx,2) + pow(delz,2)))*pow(pow(delx,2) + pow(dely,2) + pow(delz,2),-1.5)*pow(pow(oriXuni,2) + pow(oriYuni,2) + pow(oriZuni,2),-1)*(B2*(delx*oriXuni + dely*oriYuni + delz*oriZuni) + B1*pow((pow(delx,2) + pow(dely,2) + pow(delz,2))*(pow(oriXuni,2) + pow(oriYuni,2) + pow(oriZuni,2)),0.5));
              weight_factor_z[jj] = (delz*(delx*oriXuni + dely*oriYuni) - oriZuni*(pow(delx,2) + pow(dely,2)))*pow(pow(delx,2) + pow(dely,2) + pow(delz,2),-1.5)*pow(pow(oriXuni,2) + pow(oriYuni,2) + pow(oriZuni,2),-1)*(B2*(delx*oriXuni + dely*oriYuni + delz*oriZuni) + B1*pow((pow(delx,2) + pow(dely,2) + pow(delz,2))*(pow(oriXuni,2) + pow(oriYuni,2) + pow(oriZuni,2)),0.5));

              norma_x += weight_factor_x[jj];
              norma_y += weight_factor_y[jj];
              norma_z += weight_factor_z[jj];

            // Si esta fuera de rango. 
            } else {

              weight_factor_x[jj] = 0.0;
              weight_factor_y[jj] = 0.0;
              weight_factor_z[jj] = 0.0;

            }
          
          // Es particula de relleno del coloide   
          } else if (jtype == type_col_fill) {

              weight_factor_x[jj] = 0.0;
              weight_factor_y[jj] = 0.0;
              weight_factor_z[jj] = 0.0;

          // Es particula central
          } else if (jtype == type_col_cent) {  // Solo debría poder ser otra particula central

              weight_factor_x[jj] = 0.0;
              weight_factor_y[jj] = 0.0;
              weight_factor_z[jj] = 0.0;

          // Es otro tipo de particula (pared por ejemplo). De momento no implementado.
          } else {

              weight_factor_x[jj] = 0.0;
              weight_factor_y[jj] = 0.0;
              weight_factor_z[jj] = 0.0;

          }

        }// [Loop vecinos jj]


        //////////////////////////////////////////////////////////////////
        // Aplicamos las fuerzas de redist. sobre el solvente en rango. //
        //////////////////////////////////////////////////////////////////

        // Calculamos el modulo del vector resultante de sumar todos los vectores redistribuidos.
        norma = sqrt( pow(norma_x,2) + pow(norma_y,2) + pow(norma_z,2) );
        
        // DEBUG 1 printf("(core %d)\n",me);        
        // DEBUG 1 printf("\n\n(core %d)   LOOP REDIST >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n\n", me);

        for (jj = 0; jj < jnum/*+nghost*/; jj++) { // [FOR  jj]

          j = jlist[jj];    // Local idx of neighbor
          j &= NEIGHMASK;
          jtype = type[j];

          delx = x[j][0]-xtmp;
          dely = x[j][1]-ytmp;
          delz = x[j][2]-ztmp;
          rsq = delx*delx + dely*dely + delz*delz;
          r=sqrt(rsq);

          // Si la particula es de tipo solvente y esta en rango.
          if (jtype == type_solv && r < cutoff && r > Rc) { // [If in range solv particle]

            // Aplicamos la fuerza que le toca.
            f[j][0] += Pe*weight_factor_x[jj]/norma;  
            f[j][1] += Pe*weight_factor_y[jj]/norma;
            f[j][2] += Pe*weight_factor_z[jj]/norma;

            // DEBUG 1 printf("(core %d)\n",me);
            // DEBUG 1 printf("\n\n(core %d)   7.2 LOOP REDIST (jj: %d). Solv part: %d(%d) -------------\n\n", me, jj, tag[j], j);
            // DEBUG 1 printf("(core %d)     Pos   = (%f, %f, %f) \n", me, x[j][0], x[j][1], x[j][2]);
            // DEBUG 1 printf("(core %d)     Force = (%f, %f, %f) \n", me, Pe*weight_factor_x[jj]/norma, Pe*weight_factor_y[jj]/norma, Pe*weight_factor_z[jj]/norma);
            // DEBUG 1 printf("\n");

            // Buscamos la particula de coloide que esté mas cerca y le aplicamos la fuerza opuesta.

            colidx = molecule[i2]-1;
            col_part_local_idx_min = atom->map(col_atoms_tot[colidx][0]);
            rsq_solv_colpart_min = BIG;

            // DEBUG 1 printf("(core %d)\n",me);
            // DEBUG 1 printf("\n\n(core %d)   7.3 LOOP SEARCH NEAREST COL PART (min lixd: %d) ---------\n\n", me, col_part_local_idx_min);

            // DEBUG 1 printf("(core %d)   colatoms[%d][0] = %d \n", me, colidx, col_atoms_tot[colidx][0]);

            for (col_part = 0; col_part < atoms_per_colloid; ++col_part){
              
              // DEBUG 1 printf("(core %d)\n",me);              
              // DEBUG 1 printf("\n\n(core %d)   FOR >>>> ------------------------------------------------------\n\n", me);

              // Select colloid particle
              col_part_local_idx = atom->map(col_atoms_tot[colidx][col_part]);

              // Exclude center particles from redistribution.
              if (type[col_part_local_idx] != type_col_cent) {
              
                // DEBUG 1 printf("(core %d)   col part: %d,   pos: (%f, %f, %f)\n\n", me, col_part, x[col_part_local_idx][0], x[col_part_local_idx][1], x[col_part_local_idx][2]);
                // DEBUG 1 printf("(core %d)     gid:  %d\n", me, col_atoms_tot[colidx][col_part]);
                // DEBUG 1 printf("(core %d)     lixd: %d\n", me, col_part_local_idx);
                // DEBUG 1 printf("(core %d)     tag:  %d\n", me, tag[col_part_local_idx]);

                // Check its ghosts (when particle is outside subdomain col_part_local_idx = -1 ).
                while (col_part_local_idx >= 0) {

                  // DEBUG 1 printf("(core %d)\n",me);
                  // DEBUG 1 printf("\n\n(core %d)     WHILE >>>> --------------------------------------------------\n\n", me);
                  // DEBUG 1 printf("(core %d)       lixd:      %d\n", me, col_part_local_idx);

                  delx_solv_colpart = x[col_part_local_idx][0] - x[j][0];
                  dely_solv_colpart = x[col_part_local_idx][1] - x[j][1];
                  delz_solv_colpart = x[col_part_local_idx][2] - x[j][2];

                  rsq_solv_colpart = pow(delx_solv_colpart,2) + pow(dely_solv_colpart,2) + pow(delz_solv_colpart,2);
                
                  // DEBUG 1 printf("(core %d)       dist:      %f\n", me, rsq_solv_colpart);
                  // DEBUG 1 printf("(core %d)       dist min:  %f\n", me, rsq_solv_colpart_min);

                  if (rsq_solv_colpart < rsq_solv_colpart_min){

                    // DEBUG 1 printf("(core %d)\n",me);
                    // DEBUG 1 printf("\n\n(core %d)       IF >>>> -------------------------------------------------\n\n", me);

                    rsq_solv_colpart_min = rsq_solv_colpart;
                    col_part_local_idx_min = col_part_local_idx;
                    
                    // DEBUG 1 printf("(core %d)         dist min:  %f\n", me, rsq_solv_colpart_min);    
                    // DEBUG 1 printf("(core %d)         lixd min:  %d\n", me, col_part_local_idx_min);

                    // DEBUG 1 printf("\n\n(core %d)       IF <<<< -------------------------------------------------\n\n", me);
                    // DEBUG 1 printf("(core %d)\n",me);

                  } 

                  // Go to the next ghost until col_part_local_idx = -1.
                  col_part_local_idx = atom->sametag[col_part_local_idx];

                  // DEBUG 1 printf("(core %d)       lixd:  %d\n", me, col_part_local_idx);
                  // DEBUG 1 printf("\n\n(core %d)     WHILE <<<< --------------------------------------------------\n\n", me);
                  // DEBUG 1 printf("(core %d)\n",me);

                }

              }

              // check if idx < 0, then atom with tag is not present on this subdomain.
              // DEBUG 1 printf("\n\n(core %d)   FOR <<<< ------------------------------------------------------\n\n", me);
              // DEBUG 1 printf("(core %d)\n",me);

            }

            // DEBUG 1 printf("(core %d)     NEAREST COL PART: %d(%d), Molecule: %d\n", me, tag[col_part_local_idx_min], col_part_local_idx_min, molecule[col_part_local_idx_min]);
            // DEBUG 1 printf("(core %d)       Pos: (%f, %f, %f)\n", me, x[col_part_local_idx_min][0], x[col_part_local_idx_min][1], x[col_part_local_idx_min][2]);

            f[col_part_local_idx_min][0] -= Pe*weight_factor_x[jj]/norma;  
            f[col_part_local_idx_min][1] -= Pe*weight_factor_y[jj]/norma;
            f[col_part_local_idx_min][2] -= Pe*weight_factor_z[jj]/norma;            

          } // [If in range solv particle]
        } // [FOR  jj]
      } // [If type_col_cent]
    } // [If type_angle]
  } // [Loop angles]

// DEBUG 1 printf("\n\n(core %d) LOOP ANGLE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n\n", me);
// DEBUG 1 printf("(core %d)\n",me);



/////////////
// DEBUG 2 //
/////////////
////////////////////////////////////////////////////////////////////////////
/**************************************************************************

  printf("\n\n====================================================================\n");
  printf("nlocal:      %d\n", nlocal);    
  printf("nghost:      %d\n", nghost);              
  printf("natoms:      %ld\n", natoms);

  printf("\n\nCOLLOID - fill\n");
  printf("\nlidx     gid     type     x     y     z     fx     fy     fz\n");
  printf("---------------------------------------\n");

  for (i = 0; i < nlocal; ++i) {

    if (type[i] == 1) {
      printf("%d(%d)     %d     %d     %f     %f     %f     %f     %f     %f\n", i, atom->map(tag[i]), tag[i], type[i], x[i][0], x[i][1], x[i][2], f[i][0], f[i][1], f[i][2]);

      printf("\nSAME TAG (ghosts)\n");
      int idx = atom->map(tag[i]);

      printf("\n   idx     tag     type     x     y     z\n");
      while (idx >= 0) {

          printf("   %d(%d)     %d     %d     %f     %f     %f\n", idx, atom->map(tag[idx]), tag[idx], type[idx], x[idx][0], x[idx][1], x[idx][2]);
          idx = atom->sametag[idx];
          if (idx < 0) {
            printf("   outside subdomain!\n");
            printf("   %d(%d)     %d     \n", idx, atom->map(tag[idx]), tag[idx]);

            break;
          }          

      }
        
      printf("\n");

    }

        

  }

  printf("\n\nCOLLOID - cent\n");
  printf("\nlidx     gid     type     x     y     z\n");
  printf("---------------------------------------\n");

  for (i = 0; i < nlocal + nghost; ++i) {

    if (type[i] == 2) {
      printf("%d(%d)     %d     %d     %f     %f     %f\n", i, atom->map(tag[i]), tag[i], type[i], x[i][0], x[i][1], x[i][2]);

    }

  }

  printf("\n\nSOLVENT\n");
  printf("\nlidx     gid     type     x     y     z\n");
  printf("---------------------------------------\n");

  for (i = 0; i < nlocal + nghost; ++i) {
    if (type[i] == 3) {
      printf("%d(%d)     %d     %d     %f     %f     %f\n", i, atom->map(tag[i]), tag[i], type[i], x[i][0], x[i][1], x[i][2]);
    }

  }

  // Esto da seg fault con MPI. ¿Es porque cada core esta intentando acceder a atomos que estan fuera de su subdominio?
  // printf("\n\nGIDs\n");
  // printf("\ngid     lidx     type     x     y     z\n");
  // printf("---------------------------------------\n");

  // int local_idx;
  // for (i = 1; i < natoms+1; ++i) {
  //     local_idx = atom->map(i);
  //     printf("%d(%d)     %d     %d     %f     %f     %f\n", i, tag[local_idx], local_idx, type[local_idx], x[local_idx][0], x[local_idx][1], x[local_idx][2]);
  // }  

  printf("\n====================================================================\n");

/**************************************************************************/
///////////////////////////////////////////////////////////////////////////

}


