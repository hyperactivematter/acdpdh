/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   16/07/2021.
   Carlos Miguel Barriuso Gutierrez (carbarri@ucm.es)
   José Martín Roca (josema10@ucm.es) 
------------------------------------------------------------------------- */

#ifdef FIX_CLASS

FixStyle(hy/puller,FixPuller)           // CHANGE NAME

#else /* \
       */

#ifndef LMP_FIX_HY_PULLER_H             // CHANGE NAME
#define LMP_FIX_HY_PULLER_H

#include "fix.h"

namespace LAMMPS_NS {

class FixPuller : public Fix {          // CHANGE NAME
 public:
  FixPuller(class LAMMPS *, int, char **);
  ~FixPuller();
  int setmask();
  virtual void init();
  void init_list(int, class NeighList *); 
  void setup(int);
  void setup_pre_reverse(int, int);
  void pre_reverse(int, int);

 protected:
  int idrigid;        
  int type_col_cent;
  int type_col_fill;
  int type_solv;
  int type_angle;
  double cutoff, Pe, Rc;
  double B1, B2;
  class NeighList *list;
  int atoms_per_colloid;
  int num_colloids;
  int num_colloids_max;    
  int **col_atoms;
  int **col_atoms_tot;

};

}  // namespace LAMMPS_NS

#endif /* \
        */
#endif /* \
        */

    /* ERROR/WARNING messages:

    E: Illegal ... command

    Self-explanatory.  Check the input script syntax and compare to the
    documentation for the command.  You can use -echo screen as a
    command-line option when running LAMMPS to see the offending line.

    */
